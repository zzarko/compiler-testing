# Compiler-Testing

Skript za automatsko testiranje provera za Programske prevodioce
revizija 2018-01-14

## VAŽNE NAPOMENE

- skriptu je neophodna struktura direktorijuma kakva je dobijena u arhivi
- skriptu je neophodan fajl sistem koji podržava RWX prava (ne radi na NTFS/FAT fajl sistemima!)
- skriptu su neophodni sledeći programi za rad: bash (verzija 4 ili veća), grep, make, flex,
  bison, diff, fldiff (ili neki drugi vizuelni diff)
- u direktorijum 'resenje' treba da se postavi tačno rešenje zadatka, sa test fajlovima
  (ukoliko postoje podgrupe, isto treba uraditi za podgrupu b i direktorijum 'resenjeb')
- u direktorijum 'arhive' treba da se postave arhive sa studentskim rešenjima
  (fajlovi sa nazivima provera_YYYY-MM-DD_HH-MM_sXXX.tgz, raspakovani iz arhive sa rešenjima)
- u direktorijum 'micko' treba da se postavi osnovna micko ili semantic varijanta kompajlera


## PODGRUPE

Skript podržava rad sa dve podgrupe, 'a' i 'b'. Naziv direktorijuma za rešenje za podgrupu 'a'
je isti kao kada se podgrupe ne koriste. Ako se podgrupe koriste, treba u direktorijum 'resenjeb'
postaviti rešenje zadatka za podgrupu 'b' i test fajlove za taj zadatak.

Da bi skript mogao da automatski odredi podgrupu, treba postaviti promenljivu PODGRUPA (prva na
spisku iza ovog uputstva) na string:

    PODGRUPA='<podgrupa> <fajl> <kljucna_reč>'

gde je `<podgrupa>` slovo 'a' ili 'b', a `<fajl>` je fajl u kome će se tražiti `<kljucna_reč>` (može
biti i regularan izraz po `grep -E` sintaksi). Na primer:

    PODGRUPA='a micko.l loop'

će definisati da će podgrupa biti 'a' ukoliko se u fajlu 'micko.l' nalazi reč 'loop', a u suprotnom
će podgrupa biti 'b'. Preporuka je da se iskoristi .l fajl za podgrupu koja ima neku ključnu reč koju
druga podgrupa nema. Ako to nije moguće, može se upotrebiti i drugi fajl za koga se može definisati
reč ili regularni izraz koji se pojavljuje samo u jednoj podgrupi (dobar izbor je i zadatak.txt).


## PRIMERI UPOTERBE

    ./x -d 05

Raspakuje arhivu sa radnog mesta 05, pokreće testove i prikazuje vizuelni diff

    ./x

Ponovo pokreće testove sa tekućim studentom (npr, nakon izmene koda)

    ./x -n -d

Raspakuje arhivu sa sledećeg radnog mesta (ako je tekuće bilo 05, onda raspakuje 06), pokreće
testove i prikazuje vizuelni diff

    ./x -s

Ukoliko su prilikom pregleda zadatka napravljene izmene na studentskom rešenju kako bi se
ono popravilo ili videlo da li nakon uklanjanja neke greške ostatak programa radi, te izmene
će se sačuvati u `arhive/XX.patch` fajlu radi kasnije prezentacije studentu kada dođe na
uvid u rad (da se pokaže kako je rešenje trebalo da izgleda) ili čisto za evidenciju...

    ./x -a 07

Raspakuje arhivu sa radnog mesta 07 i primenjuje na njega zapamćene izmene (`arhive/07.patch`)
Nakon primene izmena, otvara vizuelni diff alat za sve izmenjene fajlove

    ./x -p 20

Zadavanje broja poena na testu. Nakon odrađenih testova će se ispisati koliko poena je student
dobio od maksimalnih 20. Ukoliko test fajlovi imaju težinske faktore u sebi, koristiće se za
računanje poena. Ukoliko težinskih faktora nema, uzeće se da svaki test nosi 1 težinski poen.

    ./csv 20

Pored `x` skripta, u direktorijumu se nalazi i `csv` skript koji pokreće testiranje za sva
radna mesta i generiše fajl sa rezultatima koji se direktno može uvesti u evidenciju.
Jedini parametar koji se mora navesti je maksimalan broj poena koji test nosi. Korišćenje
ove opcije bi trebalo da bude praćeno i težinskim faktorima u svim testovima. U principu je 
napravljeno za neku eventualnu kasniju upotrebu, zasad eksperimentalno.


## STRUKTURA TEST FAJLOVA

Nazivi test fajlova treba da prate nomenklaturu sa vežbi
(test-ok, test-sanity, test-semerr, test-synerr)

Na početku test fajla bi trebalo da stoji komentar sa opisom, npr:

    //OPIS: Sanity check za miniC gramatiku

Ovo služi da se kod pokretanja testova u terminalu može lako videti šta se testira

Test fajl može da sadrži i liniju sa težinskim faktorima, npr:

    //TEZINA 15
    //TEZINA -10
    //TEZINA 15 -5
    //TEZINA -15 30

Pozitivna vrednost će se dodavati ako je test prošao, negativna ako nije. Ukoliko
neka od vrednosti, pozitivna ili negativna, nije navedena, podrazumevaće se da je 0.
Na kraju se ispisuje i procenat ovakvih težinskih poena, ako su zadati (ukupna suma se
računa tokom prolaska kroz testove, pri čemu se sabiraju samo pozitivne vrednosti)
Ako se zada i broj poena za test (`-p`), ispisaće se i koliko bi poena bilo osvojeno.

