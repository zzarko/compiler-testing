#!/bin/bash
# skript za automatsko testiranje provera za Programske prevodioce
# revizija 2018-01-14

# VAŽNE NAPOMENE

# - skriptu je neophodna struktura direktorijuma kakva je dobijena u arhivi

# - skriptu je neophodan fajl sistem koji podržava RWX prava (ne radi na NTFS/FAT fajl sistemima!)

# - skriptu su neophodni sledeći programi za rad: bash (verzija 4 ili veća), grep, make, flex,
#   bison, diff, fldiff (ili neki drugi vizuelni diff)

# - u direktorijum 'resenje' treba da se postavi tačno rešenje zadatka, sa test fajlovima
#   (ukoliko postoje podgrupe, isto treba uraditi za podgrupu b i direktorijum 'resenjeb')
# - u direktorijum 'arhive' treba da se postave arhive sa studentskim rešenjima
#   (fajlovi sa nazivima provera_YYYY-MM-DD_HH-MM_sXXX.tgz, raspakovani iz arhive sa rešenjima)
# - u direktorijum 'micko' treba da se postavi osnovna micko ili semantic varijanta kompajlera


# PODGRUPE

# Skript podržava rad sa dve podgrupe, 'a' i 'b'. Naziv direktorijuma za rešenje za podgrupu 'a'
# je isti kao kada se podgrupe ne koriste. Ako se podgrupe koriste, treba u direktorijum 'resenjeb'
# postaviti rešenje zadatka za podgrupu 'b' i test fajlove za taj zadatak.

# Da bi skript mogao da automatski odredi podgrupu, treba postaviti promenljivu PODGRUPA (prva na
# spisku iza ovog uputstva) na string:

#   PODGRUPA='<podgrupa> <fajl> <kljucna_reč>'

# gde je <podgrupa> slovo 'a' ili 'b', a <fajl> je fajl u kome će se tražiti <kljucna_reč> (može
# biti i regularan izraz po 'grep -E' sintaksi). Na primer:

#   PODGRUPA='a micko.l loop'

# će definisati da će podgrupa biti 'a' ukoliko se u fajlu 'micko.l' nalazi reč 'loop', a u suprotnom
# će podgrupa biti 'b'. Preporuka je da se iskoristi .l fajl za podgrupu koja ima neku ključnu reč koju
# druga podgrupa nema. Ako to nije moguće, može se upotrebiti i drugi fajl za koga se može definisati
# reč ili regularni izraz koji se pojavljuje samo u jednoj podgrupi (dobar izbor je i zadatak.txt).


# PRIMERI UPOTERBE

#   ./x -d 05
# Raspakuje arhivu sa radnog mesta 05, pokreće testove i prikazuje vizuelni diff

#   ./x
# Ponovo pokreće testove sa tekućim studentom (npr, nakon izmene koda)

#   ./x -n -d
# Raspakuje arhivu sa sledećeg radnog mesta (ako je tekuće bilo 05, onda raspakuje 06), pokreće
# testove i prikazuje vizuelni diff

#   ./x -s
# Ukoliko su prilikom pregleda zadatka napravljene izmene na studentskom rešenju kako bi se
# ono popravilo ili videlo da li nakon uklanjanja neke greške ostatak programa radi, te izmene
# će se sačuvati u 'arhive/XX.patch' fajlu radi kasnije prezentacije studentu kada dođe na
# uvid u rad (da se pokaže kako je rešenje trebalo da izgleda) ili čisto za evidenciju...

#   ./x -a 07
# Raspakuje arhivu sa radnog mesta 07 i primenjuje na njega zapamćene izmene (arhive/07.patch)
# Nakon primene izmena, otvara vizuelni diff alat za sve izmenjene fajlove

#   ./x -p 20
# Zadavanje broja poena na testu. Nakon odrađenih testova će se ispisati koliko poena je student
# dobio od maksimalnih 20. Ukoliko test fajlovi imaju težinske faktore u sebi, koristiće se za
# računanje poena. Ukoliko težinskih faktora nema, uzeće se da svaki test nosi 1 težinski poen.

#   ./csv 20
# Pored 'x' skripta, u direktorijumu se nalazi i 'csv' skript koji pokreće testiranje za sva
# radna mesta i generiše fajl sa rezultatima koji se direktno može uvesti u evidenciju.
# Jedini parametar koji se mora navesti je maksimalan broj poena koji test nosi. Korišćenje
# ove opcije bi trebalo da bude praćeno i težinskim faktorima u svim testovima. U principu je 
# napravljeno za neku eventualnu kasniju upotrebu, zasad eksperimentalno.


# STRUKTURA TEST FAJLOVA

# Nazivi test fajlova treba da prate nomenklaturu sa vežbi
#   (test-ok, test-sanity, test-semerr, test-synerr)

# Na početku test fajla bi trebalo da stoji komentar sa opisom, npr:
#   //OPIS: Sanity check za miniC gramatiku
# Ovo služi da se kod pokretanja testova u terminalu može lako videti šta se testira

# Test fajl može da sadrži i liniju sa težinskim faktorima, npr:
#   //TEZINA 15
#   //TEZINA -10
#   //TEZINA 15 -5
#   //TEZINA -15 30
# Pozitivna vrednost će se dodavati ako je test prošao, negativna ako nije. Ukoliko
# neka od vrednosti, pozitivna ili negativna, nije navedena, podrazumevaće se da je 0.
# Na kraju se ispisuje i procenat ovakvih težinskih poena, ako su zadati (ukupna suma se
# računa tokom prolaska kroz testove, pri čemu se sabiraju samo pozitivne vrednosti)
# Ako se zada i broj poena za test (-p), ispisaće se i koliko bi poena bilo osvojeno.


PODGRUPA='b micko.l while'               # '<podgrupa> <fajl> <kljucna_reč>'
RESENJE=resenje/          # direktorijum sa tačnim rešenjem
ZADATAK=zadatak/          # direktorijum sa studentskim rešenjem
ZADATAKOLD=zadatak-old/   # direktorijum sa prethodnim studentskim rešenjem (radi provere prepisivanja)
ZADATAKUNM=zadatak-unm/   # direktorijum za nemodifikovan studentski zadatak
ORIGINAL=micko/           # nemodifikovani source (semantic ili micko)
HIPSIM=hipsim-src/        # source simulatora hipotetskog procesora
TEMP=/tmp/pp/             # direktorijum za privremeno raspakivanje
ARHIVE=arhive/            # direktorijum sa svim arhivama (ovde treba raspakovati sve-MIA2*.tar)
GRUPA=""                  # a ili b grupa
GRUPATXT=""               # tekst za ispis grupe

TCORRECT=0                # ukupan broj ispravnih testova
TSYNTAX=0                 # ukupan broj testova sa sintaksnim greškama
TSEMANTIC=0               # ukupan broj testova sa semantičkim greškama
ERRORS=0                  # broj pogrešnih prevođenja testova
NOERRORSM=0               # broj pogrešnih prevođenja testova sa semantičkim greškama
NOERRORSX=0               # broj pogrešnih prevođenja testova sa sintaksnim greškama
TERMINATIONS=0            # broj nasilnih prekida testova
WARNINGS=0                # uspešno prevođenje kompajlera
CONFLICTS=0               # više od jednog konflikta
WRONGGEN=0                # broj pogrešnih generisanih fajlova
SANITY=1                  # da li je pokvareno originalno generisanje koda

DIFFPRG=fldiff            # grafički program za upoređivanje (kompare, xxdiff,...)
DIFF=true                 # da li će se koristiti vizuelni diff ili ne
EXECUTABLE=a.out          # naziv izvršnog fajla kompajlera (postavlja se automatski)
SIMULATOR=a.out           # naziv izvršnog fajla simulatora (postavlja se automatski)
SIMSTEPS=300              # maksimalan broj koraka za simulaciju
KEEP=0                    # da li zadržati tekući zadatak direktorijum
SILENT=0                  # da li ispisati samo finalni procenat (za csv fajl)
MAXPOINTS=0               # broj poena koje nosi zadatak (za csv fajl)
SPATCH=0                  # da li se kreira patch
LPATCH=0                  # da li se primenjuje patch
NEXTST=0                  # da li se prelazi na sledećeg sudenta
NEXTSTINC=1               # inkrement za sledeće radno mesto
WEIGHT=0                  # težina tekućeg testa
TOTALWEIGHT=0             # ukupna težina testova
POINTS=0                  # ukupan broj osvojenih poena

#hvatanje glibc grešaka na stderr umesto direktno na ekran
export LIBC_FATAL_STDERR_=1

# ispis poruka u boji
# - string za ispis sa [r],[g],[b],[m],[c],[n]
# - ako se zada -c NN, tekst će se centrirati na NN karaktera
# - ako se zada -n, neće se ispisati newline nakon ispisa
function colecho {
    if [ $SILENT -eq 0 ]; then
        nonewl=0
        width=0
        if [ "$1" == "-c" ]; then
            width=$2
            shift 2
        fi
        if [ "$1" == "-n" ]; then
            nonewl=1
            shift
        fi
        output="$1"
        chars=${output//\[[a-z]\]/}
        if [ "$2" == "" ]; then output="${output}\e[00m"; fi
        output=${output//\[r\]/\\e\[01;31m}
        output=${output//\[g\]/\\e\[01;32m}
        output=${output//\[b\]/\\e\[01;34m}
        output=${output//\[m\]/\\e\[01;35m}
        output=${output//\[c\]/\\e\[01;36m}
        output=${output//\[n\]/\\e\[00m}
        if [ "$width" != "0" ]; then
            textsize=${#chars}
            if [ $width -gt $textsize ]; then
                span=$((($width - $textsize) / 2))
                printf "%${span}s" ""
            fi
        fi
        if [ $nonewl -eq 0 ]; then
            echo -e "$output"
        else
            echo -n -e "$output"
        fi
    fi
}

function usage {
    colecho "[b]Script for automatic testing of miniC exams"
    colecho "\nUsage: [b]$0[n] [g][OPTIONS] [c][PLACE]"
    colecho "\nWithout arguments - start testing with current files"
    colecho "If [c]PLACE[n] [[c]00-31[n]] is given, unpack archive for that workplace and start testing"
    colecho "\nOptions:"
    colecho "[g]-n[n]            Unpack the archive of next student and start testing"
    colecho "[g]-b[n]            next student is 2 workplaces away (if only looking A/B subgroup)"
    colecho "[g]-d[n]            Display visual diff of student and original files"
    colecho "[g]-s[n]            Save changes made by you to a patch file in 'arhive' directory"
    colecho "[g]-a[n] <PLACE>    Apply saved patch to archive from workplace PLACE [00-31], and show differences"
    colecho "[g]-p[n] <POINTS>   POINTS is max number of points for test"
    colecho "[g]-q[n]            Quiet testing (for CSV creation), must be used with -p"
    colecho "[g]-c[n]            Clean all temporary files"
    colecho "[g]-h[n]            This help"
}

# sprovođenje jednog testa
# $1 - naziv fajla
# $2 - boja ispisa zaglavlja
# $3 - provera izlaza
function do_test {
    if [ -f "$1" ]; then
        # odredi težinu testa
        WEIGHT=$(grep -m 1 "//TEZINA" $1 | grep -oE "[+-]?[0-9]+")
        WEIGHT=$(echo $WEIGHT)
        NEGWEIGHT=${WEIGHT##* }
        WEIGHT=${WEIGHT%% *}
        if [ "$WEIGHT" == "" ]; then WEIGHT=0; fi
        if [ "$NEGWEIGHT" == "" ]; then NEGWEIGHT=0; fi
        if [ $WEIGHT -le 0 ] && [ $NEGWEIGHT -ge 0 ]; then
            w=$WEIGHT
            WEIGHT=$NEGWEIGHT
            NEGWEIGHT=$w
        fi
        if [ $WEIGHT -lt 0 ]; then WEIGHT=0; fi
        if [ $NEGWEIGHT -gt 0 ]; then NEGWEIGHT=0; fi
        TOTALWEIGHT=$((TOTALWEIGHT+WEIGHT))

        colecho "\n\n***********************************************************"
        colecho -n "$2$1, W:$WEIGHT,$NEGWEIGHT : "
        if [ $SILENT -eq 0 ]; then grep -m 1 "//OPIS.*" $1; fi
        if [ $? -ne 0 ]; then
            colecho
        fi
        colecho "[n]***********************************************************"
        cd $RESENJE
        colecho "[c]CORRECT:"
        #pamćenje izlaza tačnog rešenja
        outt=$(./$EXECUTABLE < ../$1 2>&1)
        OUTT=$?
        outt="$(echo -e "${outt}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')" #trim
        if [ "$outt" != "" ]; then
            colecho "$outt"
        fi
        if [ $OUTT -ne 0 ] && [ -f output.asm ]; then rm output.asm; fi
        cd ../$ZADATAK
        colecho "[c]\nSTUDENT:"
        #pamćenje izlaza studentskog rešenja
        outs=$(./$EXECUTABLE < ../$1 2>&1)
        OUTS=$?
        outs="$(echo -e "${outs}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')" #trim
        if [ "$outs" != "" ]; then
            colecho "$outs"
        fi
        cd ..

        #provera povratne vrednosti
        if [[ $OUTS -gt 127 && $OUTS != 255 ]]; then
            colecho "[r]\nCompiler terminated - Segmentation fault!"
            if [[ $OUTT -gt 127 && $OUTT != 255 ]]; then
                colecho "[g]\nBut, that is OK?\n"
            else
                TERMINATIONS=$((TERMINATIONS + 1))
                POINTS=$((POINTS + NEGWEIGHT))
            fi
        fi
        if [[ "$1" =~ .*semerr.* ]]; then
            TSEMANTIC=$((TSEMANTIC + 1))
            if [[ $OUTS -gt 127 || $OUTS == 0 ]]; then
                if [[ $OUTS == 0 ]]; then
                    colecho "[r]\nError not reported!"
                elif [[ $OUTS == 255 ]]; then
                    colecho "[r]\nSyntax error reported!"
                fi
                if [[ $OUTT -gt 127 && $OUTT != 255 && $OUTS -gt 127 && $OUTS != 255 ]]; then
                    #sve je OK, i originalno rešenje je puklo :)
                    POINTS=$((POINTS + WEIGHT))
                else
                    NOERRORSM=$((NOERRORSM + 1))
                    POINTS=$((POINTS + NEGWEIGHT))
                fi
            else
                POINTS=$((POINTS + WEIGHT))
            fi
        elif [[ "$1" =~ .*synerr.* ]]; then
            TSYNTAX=$((TSYNTAX + 1))
            if [[ $OUTS != 255 ]]; then
                colecho "[r]\nSyntax error not reported!"
                NOERRORSX=$((NOERRORSX + 1))
                POINTS=$((POINTS + NEGWEIGHT))
            else
                POINTS=$((POINTS + WEIGHT))
            fi
        else
            TCORRECT=$((TCORRECT + 1))
            if [ $OUTS -ne 0 ]; then
                ERRORS=$((ERRORS + 1))
                if [[ "$1" =~ .*sanity.* ]]; then
                    colecho "[r]\nOriginal grammar is changed!"
                    if [ $OUTT -ne 0 ] && [ $OUTS -ne 255 ]; then
                        colecho "[g]\nBut it is also for correct solution..."
                        ERRORS=$((ERRORS - 1))
                    else
                        SANITY=0
                    fi
                else
                    if [ $OUTS -eq 255 ]; then
                        colecho "[r]\nSyntax error reported for correct program!"
                    else
                        colecho "[r]\nError reported for correct program!"
                    fi
                fi
                #ako nije generisanje koda, ovde računaj poene
                if [ $3 -eq 0 ] || [ ! -f $RESENJE/output.asm ]; then
                    POINTS=$((POINTS + NEGWEIGHT))
                fi
                rm -f $ZADATAK/output.asm 2>/dev/null
            else
                #ako nije generisanje koda, ovde računaj poene
                if [ $3 -eq 0 ] || [ ! -f $RESENJE/output.asm ]; then
                    POINTS=$((POINTS + WEIGHT))
                fi
            fi
        fi
        colecho

        #provera generisanog koda (samo ako je zadatak sa generisanjem koda i ako su testovi bez grešaka)
        if [ $3 -ne 0 ] && [ -f $RESENJE/output.asm ]; then
            asmname=$(basename $1)
            asmname="${asmname%.*}.asm"
            mv $RESENJE/output.asm $RESENJE/$asmname
            if [ -f $ZADATAK/output.asm ]; then
                mv $ZADATAK/output.asm $ZADATAK/$asmname
                diff -w -B $RESENJE/$asmname $ZADATAK/$asmname 2>/dev/null 1>/dev/null
                if [ $? -eq 0 ]; then
                    colecho "[g]\nASM files are identical"
                    POINTS=$((POINTS + WEIGHT))
                else #provera izvršavanja izgenerisanog koda
                    colecho "[b]\nASM files are different, running simulator..."
                    #provera sintakse studentskog rešenja
                    outs=$($HIPSIM/$SIMULATOR -c < $ZADATAK/$asmname 2>&1)
                    OUTS=$?
                    if [ $OUTS != 0 ]; then
                        #if [ "${1:5:6}" == "sanity" ]; then
                        if [[ "$1" =~ .*sanity.* ]]; then
                            colecho "[r]\nCode generation for original grammar is corrupted!"
                            SANITY=0
                        else
                            colecho "\nASM file [b]$asmname[n] [r]has errors!"
                            echo "$outs"
                        fi
                        WRONGGEN=$((WRONGGEN + 1))
                        ERRORS=$((ERRORS + 1))
                        POINTS=$((POINTS + NEGWEIGHT))
                        $DIFF $RESENJE/$asmname $ZADATAK/$asmname 1>/dev/null 2>/dev/null
                    else
                        outt=$($HIPSIM/$SIMULATOR -r -s $SIMSTEPS < $RESENJE/$asmname 2>&1)
                        OUTT=$?
                        outs=$($HIPSIM/$SIMULATOR -r -s $SIMSTEPS < $ZADATAK/$asmname 2>&1)
                        OUTS=$?
                        if [ "$outt" != "$outs" ]; then
                            #if [ "${1:5:6}" == "sanity" ]; then
                            if [[ "$1" =~ .*sanity.* ]]; then
                                colecho "[r]\nCode generation for original grammar is corrupted!"
                                SANITY=0
                            else
                                if [ $OUTS -eq 0 ]; then
                                    colecho "\nASM file [b]$asmname[n] [r]does not give correct result!"
                                    outs=$(echo $outs) #trim
                                    colecho "It should be [g]$outt[n], but it gives [r]$outs"
                                elif [ $OUTS -eq 4 ]; then
                                    colecho "[r]\nSimulation didn't finish after $SIMSTEPS steps!"
                                else
                                    colecho "[r]\nSimulation didn't finish:[n]\n$outs"
                                fi
                            fi
                            WRONGGEN=$((WRONGGEN + 1))
                            ERRORS=$((ERRORS + 1))
                            POINTS=$((POINTS + NEGWEIGHT))
                            $DIFF $RESENJE/$asmname $ZADATAK/$asmname 1>/dev/null 2>/dev/null
                        else
                            colecho "\nASM file [b]$asmname[n] [g]gives correct result ($outt)"
                            POINTS=$((POINTS + WEIGHT))
                        fi
                    fi
                fi
            else
                #if [ "${1:5:6}" == "sanity" ]; then
                if [[ "$1" =~ .*sanity.* ]]; then
                    colecho "[r]\nOriginal grammar is corrupted!"
                    SANITY=0
                else
                    colecho "[r]\nThere is no ASM file $asmname!"
                fi
                WRONGGEN=$((WRONGGEN + 1))
                POINTS=$((POINTS + NEGWEIGHT))
            fi
        fi
    fi
    #colecho "P:[b]$POINTS[n],W:$WEIGHT,NW:$NEGWEIGHT" #TODO: debug
}

# $1 - radno mesto
# $2 - direktorijum za raspakivanje
# $3 - direktorijum za kopiju
function unpack_student {
    # kreiraj temp direktorijum ako ne postoji
    if [ ! -d "$TEMP" ]; then
        mkdir "$TEMP"
    fi

    # brisanje direktorijuma pre raspakvanja
    rm -rf "$2"/* 2>/dev/null
    rm -rf "$3"/* 2>/dev/null
    echo $1 > $2/student-sNN

    # izvlačenje podataka iz spisak fajla
    spline=$(grep -E "s[0-9]$1" $ARHIVE/spisak*)
    spline=${spline#*, }
    sindex=${spline%%,*}
    sname=${spline##*, }
    #echo "#$sindex#$sname#"
    if [ "$sindex" == "" ]; then
        colecho "[r]\nNo one was working on $1"
        exit 1
    fi

    #raspakivanje arhive; podrazumeva se da je rešenje u /home/provera/br_indeksa
    tar -C "$TEMP"/ -xzf "$ARHIVE"/provera_*_s?$1.tgz #2>/dev/null
    OUT=$?
    if [ $OUT -ne 0 ]; then
        colecho "[r]Something went wrong with unpacking, exit code $OUT..."
        exit 1
    fi
    mv "$TEMP/home/provera/$sindex"/* "$2"/ 2>/dev/null
    if [ $? -ne 0 ]; then
        colecho "[r]Something is wrong with student archive..."
        exit 1
    fi

    #kreiraj fajlove sa indeksom, imenom i radnim mestom studenta
    rm -rf "$TEMP"/*
    echo $sindex > $2/student-index
    echo $sname > $2/student-name

    #ime i indeks studenta iz fajla sa rešenjem (ako je ostavio/la)
    ssname=$(grep -m 1 -i "autor" $ZADATAK/$PARSER.y)
    idxreg="e[0-9]{4,5}|[a-z]{1,3}[[:space:]/\-]*[0-9]{1,4}[[:space:]/\-]20[0-9]{2}|[a-z]{1,3}[[:space:]/\-]*[0-9]{4,8}"
    if [ $? -ne 0 ]; then
        ssname=$(grep -iE -m 1 "//.*($idxreg).*" $ZADATAK/$PARSER.y)
    fi
    #echo a $ssname
    ssname=${ssname#*AUTOR}
    #echo b $ssname
    ssname=${ssname#*autor}
    #echo c $ssname
    ssname=${ssname/:/}
    ssname=${ssname/,/}
    ssname=${ssname/INDEX/}
    ssname=${ssname/index/}
    ssname=${ssname/INDEKS/}
    ssname=${ssname/indeks/}
    #echo d $ssname
    ssindex=$(echo $ssname | grep -oiE $idxreg)
    ssindex=${ssindex/\//-}
    ssindex=${ssindex/\\/-}
    ssindex=${ssindex,,}     #lower
    ssindex=$(echo $ssindex) #trim
    #echo e $idx
    ssname=${ssname/$ssindex/}
    #echo f $ssname
    ssname=$(echo $ssname)
    #echo g "#$ssname#"
    #echo $ssname > $2/student-name
    if [ "$sindex" != "$ssindex" ]; then
        colecho "\nStudent [b]$sname, $sindex[n] left this as his/hers info: [m]$ssname, $ssindex"
    fi

    #komentarisanje poziva print_symtab()
    sed -i 's/print_symtab();/\/\*print_symtab();\*\//g' "$2"/$PARSER.y

    # određivanje grupe, a ili b
    if [ "$PODGRUPA" == "" ]; then
        GRUPA="a"
    else
        GRUPA=${PODGRUPA%% *}
        GRUPA=${GRUPA,,}
        #echo "grupa:#$GRUPA#"
        PODGRUPA=${PODGRUPA#* }
        fajl=${PODGRUPA%% *}
        #echo "fajl:#$fajl#"
        razl=${PODGRUPA##* }
        #echo "razl:#$razl#"
        grep -ciE "$razl" "$ZADATAK"/$fajl 1>/dev/null 2>/dev/null
        if [ $? -ne 0 ]; then
            if [ "$GRUPA" == "a" ]; then
                GRUPA="b"
            else
                GRUPA="a"
            fi
        fi
    fi
    echo $GRUPA>"$ZADATAK"/student-grupa
    if [ -f "$(basename $RESENJE)b"/defs.h ]; then GRUPATXT=", Grupa ${GRUPA^^}"; fi
    if [ "$GRUPA" == "a" ]; then GRUPA=""; fi
    RESENJE="$(basename $RESENJE)$GRUPA/"

    #čišćenje studentskog rešenja
    cd "$2"
    make clean 1>/dev/null 2>/dev/null
    cd ..

    #postavi vreme svih fajlova na trenutno, kako bi tekst editor skontao da je fajl izmenjen
    touch "$2"/*

    #izmeni main funkciju u studentskom zadatku
    #patch $ZADATAK/$PARSER.y maindiff

    # iskopiraj fajlove i u direktorijum za kopiju
    cp "$2"/* "$3"/ 1>/dev/null 2>/dev/null
}

# proveri prosleđene opcije
while getopts ":hcsnbdqa:p:" opt; do
  case $opt in
    h)
        # prikaži uputstvo
        usage
        exit 0
        ;;
    c)
        # pobriši sve privremene fajlove
        if [ -d $HIPSIM ]; then
            colecho "Cleaning [b]hipsim...[n]"
            cd $HIPSIM
            make clean
            cd ..
        fi
        colecho "Cleaning [b]original compiler files...[n]"
        cd $ORIGINAL
        make clean
        cd ..
        colecho "Cleaning [b]solution...[n]"
        cd $RESENJE
        make clean
        cd ..
        if [ -f "$(basename $RESENJE)b/defs.h" ]; then
            colecho "Cleaning [b]group B solution...[n]"
            cd "$(basename $RESENJE)b"
            make clean
            cd ..
        fi
        colecho "Cleaning [b]backup mc files...[n]"
        rm -rf "$ZADATAK"/* 2>/dev/null
        rm -rf "$ZADATAK"/.[a-zA-Z0-9]* 2>/dev/null
        rm -rf "$ZADATAKOLD"/* 2>/dev/null
        rm -rf "$ZADATAKUNM"/* 2>/dev/null
        rm *.mc~ 2>/dev/null
        exit 0
        ;;
    s)
        # sačuvaj patch tekućih fajlova u odnosu na originalne studentove
        SPATCH=1
        LPATCH=0
        KEEP=1
        ;;
    n)
        # sledeći student
        NEXTST=1
        ;;
    b)
        # inkrement za sledeće mesto
        NEXTSTINC=2
        ;;
    d)
        # otvaraj vizuelni diff
        DIFF=$DIFFPRG
        ;;
    q)
        # tihi režim, za generisanje CSV fajla
        SILENT=1
        ;;
    a)
        # raspakuj arhivu i primeni sačuvani patch
        LPATCH=$OPTARG
        SPATCH=0
        KEEP=0
        DIFF=$DIFFPRG
        ;;
    p)
        # definiši broj poena za test
        MAXPOINTS=$OPTARG
        ;;
    \?)
        echo "Invalid option: -$OPTARG" >&2
        exit 1
        ;;
    :)
        echo "Option -$OPTARG requires an argument." >&2
        exit 1
        ;;
  esac
done
shift $((OPTIND-1))

# prelazak na sledeće radno mesto
if [ $NEXTST -ne 0 ]; then
    if [ -f "$ZADATAK"/student-sNN ]; then
        place=$(cat "$ZADATAK"/student-sNN)
        place=${place##0}
        place=$((place + $NEXTSTINC))
        if [ $place -lt 32 ]; then
            place=$(printf "%02d" $place)
            eval set -- $place
        else
            colecho "[b]You are on the last workplace..."
            exit 0
        fi
    else
        eval set -- "00"
    fi
fi

if [ "$LPATCH" != "0" ]; then
    eval set -- $LPATCH
    LPATCH=1
fi

# ako nema argumenta, radi sa tekućim zadatak direktorijumom
if [ "$1" == "" ]; then KEEP=1; fi

#zanemari vizuelni diff u silent režimu
if [ $SILENT -ne 0 ]; then
    DIFF=true
    if [ $MAXPOINTS -eq 0 ]; then
        SILENT=0
        colecho "[r]You need to use -p option with this!"
        exit 1
    fi
fi

# nađi kako se zove fajl sa parserom
PARSER=$(basename $(ls $ORIGINAL/*.y))
PARSER="${PARSER%.*}"

# pogasi diff
killall -9 $DIFF 2>/dev/null

if [ $LPATCH -ne 0 ] || [ $KEEP -eq 0 ]; then
    # čuva se prethodni zadatak radi provere prepisivanja
    rm -rf "$ZADATAKOLD"/*
    mv "$ZADATAKUNM"/* "$ZADATAKOLD"/ 2>/dev/null

    unpack_student "$1" "$ZADATAK" "$ZADATAKUNM"

    #provera da li su tekući i prethodni sedeli jedno do drugog
    #TODO: ovo treba bolje rešiti...
    
    stud_sold=$(cat "$ZADATAKOLD"/student-sNN 2>/dev/null)
    curg=$(cat "$ZADATAK"/student-grupa 2>/dev/null)
    oldg=$(cat "$ZADATAKOLD"/student-grupa 2>/dev/null)
    #stud_sold=${stud_sold##+(0)}
    stud_sold=2$stud_sold
    stud_scur=2$1
    if [ "$oldg" == "$curg" ] && [ "$stud_sold" != "" ]; then
        sused=$(($stud_scur - $stud_sold))
        sused=${sused#-}
    else
        sused=0
    fi

    if [ $LPATCH -ne 0 ]; then
        if [ -f "$ARHIVE"/"$1.patch" ]; then
            cp "$ARHIVE"/"$1.patch" "$ZADATAK"/
            cd "$ZADATAK"/
            out=$(patch -p1 -i "$1.patch" 2>&1)
            OUT=$?
            cd ..
            rm "$ZADATAK"/"$1.patch"
            if [ $OUT -ne 0 ]; then
                colecho "[r]Something is wrong with patch:[n]\n$out"
                exit 1
            fi
            touch "$ZADATAK"/student-patch
            colecho "[g]Files patched, showing differences..."

            # prikaži sve izmene
            cd "$ZADATAK"
            for i in *.y *.l *.c *.h Makefile; do
                diff -w -B "$i" ../"$ZADATAKUNM"/"$i" 1>/dev/null 2>/dev/null
                if [ $? -ne 0 ]; then
                    $DIFF "$i" ../"$ZADATAKUNM"/"$i" &
                fi
            done
            cd ..
            DIFF=true
            #exit 0
        else
            colecho "[r]Patch file $ARHIVE/$1.patch not found!"
            exit 1
        fi
    fi
else
    # odredi podatke za studenta iz zapamćenih vrednosti
    stud=$(cat "$ZADATAK"/student-sNN)
    eval set -- "$stud"
    sindex=$(cat "$ZADATAK"/student-index)
    sname=$(cat "$ZADATAK"/student-name)
    if [ "$sindex" == "$sname" ]; then sname=""; fi
    sused=0
    if [ -f "$ZADATAK"/student-patch ]; then LPATCH=1; fi
    GRUPA=$(cat "$ZADATAK"/student-grupa)
    if [ -f "$(basename $RESENJE)b"/defs.h ]; then GRUPATXT=", Grupa ${GRUPA^^}"; fi
    if [ "$GRUPA" == "a" ]; then GRUPA=""; fi
    RESENJE="$(basename $RESENJE)$GRUPA/"
fi

# kreiranje patch-a
if [ $SPATCH -ne 0 ]; then
    if [ ! -f "$ZADATAK"/$PARSER.y ]; then
        colecho "[r]Unpack an archive first..."
        exit 1
    fi
    cd $ZADATAK
    make clean 1>/dev/null 2>/dev/null
    cd ..
    diff -c $ZADATAKUNM $ZADATAK > $ARHIVE/"$1.patch"
    colecho "[g]Created patch file: $ARHIVE/$1.patch"
    exit 0
fi

#nađi izvršni fajl simulatora
if [ -d "$HIPSIM" ]; then
    SIMULATOR=$(basename $(find $HIPSIM/ -maxdepth 1 -type f -executable) 2>/dev/null)
    if [ ! -f "$HIPSIM/$SIMULATOR" ]; then
        colecho "[b]Compiling simulator...\n"
        cd "$HIPSIM"
        if [ $SILENT -eq 0 ]; then
            make clean
            make
        else
            make clean 1>/dev/null 2>/dev/null
            make 1>/dev/null 2>/dev/null
        fi
        cd ..
        SIMULATOR=$(basename $(find $HIPSIM/ -maxdepth 1 -type f -executable))
    fi
fi

#nađi naziv izvršnog fajla zadatka
if [ "$SIMULATOR" != "" ]; then
    EXECUTABLE=$(basename $(find $RESENJE/ -maxdepth 1 -type f -executable | grep -v "$SIMULATOR") 2>/dev/null)
else
    EXECUTABLE=$(basename $(find $RESENJE/ -maxdepth 1 -type f -executable) 2>/dev/null)
fi
if [ ! -f "$RESENJE/$EXECUTABLE" ]; then
    colecho "[b]Compiling solution...\n"
    cd "$RESENJE"
    if [ $SILENT -eq 0 ]; then
        make clean 
        make
    else
        make clean 1>/dev/null 2>/dev/null
        make 1>/dev/null 2>/dev/null
    fi
    cd ..
    EXECUTABLE=$(basename $(find $RESENJE/ -maxdepth 1 -type f -executable))
fi

#glavni program
colecho "\n\n\n\n\n\n\n\n"
colecho "[g]XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
colecho -c 60 "[g]START $1$GRUPATXT"


if [ $LPATCH -ne 0 ]; then
    colecho -c 60 "[m]patched version"
else
    # da li je postojao patch koji nije primenjen?
    if [ $LPATCH -eq 0 ] && [ -f "$ARHIVE/$1.patch" ]; then
        colecho -c 60 "[m]unapplied patch exists"
    fi
fi
if [ "$sname" == "" ]; then
    colecho -c 60 "[b]$sindex"
else
    colecho -c 60 "[b]$sname - $sindex"
fi
colecho "[g]XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n"
if [ -f "$ZADATAK/Makefile" ]; then

    #kompajliranje
    cd $ZADATAK
    make clean 1>/dev/null 2>/dev/null
    if [ $SILENT -eq 0 ]; then ls --color; fi
    colecho
    make_out=$(make 2>&1)
    MAKE_STATUS=$?
    echo "$make_out" > .make.out
    if [ $SILENT -eq 0 ]; then echo "$make_out"; fi
    #provera da li je bilo konflikata u gramatici
    out=$(grep -i conflicts .make.out)
    if [ "$out" != "" ]; then
        colecho "[r]\nThere are conflicts in grammar!"
        CONFLICTS=1
    fi
    #provera da li ima upozorenja u C kodu
    out=$(grep -i -c warning .make.out)
    if [ "$out" != "0" ]; then
        colecho "[r]\nThere were warnings during compilation!"
        WARNINGS=$(($WARNINGS + $out))
    fi
    rm .make.out 1>/dev/null 2>/dev/null
    cd ..

    #prikaz izmena koje je student uradio na parseru
    $DIFF $ORIGINAL/$PARSER.y $ZADATAK/$PARSER.y & 1>/dev/null 2>/dev/null &

    #ako je bilo grešaka pri prevođenju, prekini
    if [ $MAKE_STATUS -gt 0 ]; then
        colecho "[r]\nMake error!"
        if [ -f "$ZADATAK/$EXECUTABLE" ]; then
            colecho "[b]Executable exists, trying tests anyway..."
        else
            echo "$sindex,$sname,,0,$MAXPOINTS,,"
            exit 1
        fi
    fi

    #pokretanje testova
    colecho "\n[b]TESTS:"
    for test in ${RESENJE}/test-sanity*.mc; do
        do_test $test "[g]" 1
    done
    for test in ${RESENJE}/test-ok*.mc; do
        do_test $test "[g]" 1
    done
    for test in ${RESENJE}/test-synerr*.mc; do
        do_test $test "[m]" 0
    done
    for test in ${RESENJE}/test-semerr*.mc; do
        do_test $test "[b]" 0
    done

    if [ $sused -eq 1 ] && [ "$DIFF" != "true" ]; then
        colecho "\n\n***********************************************************"
        colecho "[b]Showing differences from previous workplace..."
        colecho "***********************************************************"
        $DIFF $ZADATAKOLD/$PARSER.y $ZADATAK/$PARSER.y &
    fi
fi

# ispis procenta uspelih testova
# $1 - broj testova koji nisu prošli
# $2 - ukupan broj testova
# $3 - naziv za testove
function print_percent {
    if [ $2 -ne 0 ]; then
        PASSED=$(($2-$1))
        PERCENT=$((PASSED * 100 / $2))
        if [ $1 == 0 ]; then
            PCOL="[g]"
        else
            PCOL="[r]"
        fi
        colecho "Passed $PCOL$PASSED[n] of [g]$2[n] $3test(s) - $PCOL$PERCENT%"
    fi
}

#izveštaj
colecho "\n[g]***********************************************************"
colecho -c 60 "[g]SUMMARY $1$GRUPATXT"
if [ $LPATCH -ne 0 ]; then
    colecho -c 60 "[m]patched version"
else
    # da li je postojao patch koji nije primenjen?
    if [ $LPATCH -eq 0 ] && [ -f "$ARHIVE/$1.patch" ]; then
        colecho -c 60 "[m]unapplied patch exists"
    fi
fi
if [ "$sname" == "" ]; then
    colecho -c 60 "[b]$sindex"
else
    colecho -c 60 "[b]$sname - $sindex"
fi
colecho "[g]***********************************************************"
OK=1
if [ $CONFLICTS -gt 0 ]; then OK=0
    colecho "[r]Conflicts in grammar!"
fi
if [ $SANITY -eq 0 ]; then OK=0
    colecho "(Code generation for) original grammar is [r]CHANGED!"
fi
if [ $WARNINGS -gt 0 ]; then OK=0
    colecho "Program was compiled with [r]$WARNINGS warning(s)"
fi

print_percent $ERRORS $TCORRECT "correct "

print_percent $NOERRORSX $TSYNTAX "syntax error "

print_percent $NOERRORSM $TSEMANTIC "semantic error "

if [ $TERMINATIONS -gt 0 ]; then OK=0
    colecho "[r]$TERMINATIONS[n] terminated test(s)"
fi
if [ $WRONGGEN -gt 0 ]; then OK=0
    colecho "[r]$WRONGGEN[n] wrong/missing assembly output(s)"
fi

TTESTS=$((TCORRECT + TSEMANTIC + TSYNTAX))
TWRONG=$((ERRORS + NOERRORSX + NOERRORSM))
colecho -n "\nOverall result of automatic checks: "
print_percent $TWRONG $TTESTS ""

# ako testovi nisu bodovani, uzmi: jedan test - jedan težinski poen
PCOL="[g]"
if [ $TOTALWEIGHT -eq 0 ]; then
    TOTALWEIGHT=$TTESTS
    POINTS=$((TTESTS-TWRONG))
    PERCENT=$((POINTS * 100 / TOTALWEIGHT))
    if [ $POINTS -lt $TTESTS ]; then PCOL="[r]"; fi
else
    # u suprotnom, računaj po težinskim faktorima
    PERCENT=$((POINTS * 100 / TOTALWEIGHT))
    if [ $PERCENT -lt 0 ]; then PERCENT=0; fi
    if [ $POINTS -lt $TOTALWEIGHT ]; then PCOL="[r]"; fi
    colecho "Acquired $PCOL$POINTS[n] of total [g]$TOTALWEIGHT[n] weighted test points - $PCOL$PERCENT%"
fi
# ako je zadat broj poena kolokvijuma, ispiši koliko je osvojeno
if [ $MAXPOINTS -gt 0 ]; then
    points=$(( ($PERCENT * $MAXPOINTS + 50) / 100))
    colecho "That translates to $PCOL$points[n] out of maximum [g]$MAXPOINTS[n] points for this exam"
fi

# ako se generiše csv fajl
if [ $SILENT -ne 0 ]; then
    points=$(( ($PERCENT * $MAXPOINTS + 50) / 100))
    echo "$sindex,$sname,,$points,$MAXPOINTS,,"
fi

colecho "\n\n\n"

